#!/usr/bin/env python3

import subprocess
from flask import Flask, request, render_template, redirect, url_for, flash

app = Flask(__name__)

# Secret key for session management
app.secret_key = 'your_secret_key'

@app.route('/')
def home():
    return render_template('home.html', title='Home Page')

@app.route('/admin-login', methods=['GET', 'POST'])
def admin_login():
    if request.method == 'POST':
        # Retrieve submitted username and password
        submitted_username = request.form['username']
        submitted_password = request.form['password']

        # Use subprocess to read username and password from secret.txt
        command = "cat secret.txt"
        data = subprocess.check_output(command, shell=True).decode('utf-8').splitlines()
        
        if len(data) >= 2:
            # Assuming the first line is the username and the second line is the password
            file_username = data[0]
            file_password = data[1]

            # Check if the submitted credentials match those in the secret.txt file
            if submitted_username == file_username and submitted_password == file_password:
                # Login successful
                flash('Login successful.', 'success')
                return redirect(url_for('admin_dashboard'))
            else:
                # Login failed
                flash('Invalid username or password.', 'danger')
                return redirect(url_for('admin_login'))
        else:
            # Improperly formatted secret.txt file
            flash('Authentication system error.', 'danger')
            return redirect(url_for('admin_login'))
            
    # Render the admin login form
    return render_template('admin_login.html')

@app.route('/admin-dashboard')
def admin_dashboard():
    # Placeholder for the admin dashboard page
    return 'Welcome to the Admin Dashboard!'

@app.route('/about')
def about():
    return render_template('about.html', title='About Us')

@app.route('/contact', methods=['GET'])
def contact():
    return render_template('contact.html', title='Contact Us')

@app.route('/submit-contact-form', methods=['POST'])
def submit_contact_form():
    name = request.form['name']
    email = request.form['email']
    message = request.form['message']
    # Process the form data (e.g., save it to a database or send an email)
    # For now, just print it to the console
    print(f"Name: {name}, Email: {email}, Message: {message}")
    # Redirect to a new page or back to the contact page with a success message
    return redirect(url_for('contact'))

if __name__ == '__main__':
    app.run(debug=True)