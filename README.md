# Seamless DevSecOps - Building and Deploying a Cloud-Native Application with GitLab

## Description

This project demonstrates the end-to-end lifecycle of a cloud-native application utilizing GitLab's integrated DevOps platform. It showcases how seamless collaboration between development, security, and operations leads to faster, more secure software delivery. Through this demo, we explore the use of GitLab CI/CD pipelines for automated testing, building, and deploying, integrated with comprehensive security scanning to ensure code quality and security compliance. Participants will gain insights into leveraging GitLab for project planning, source code management, continuous integration (CI), continuous deployment (CD), and monitoring within a DevSecOps framework. Our goal is to highlight the efficiency and effectiveness of GitLab in facilitating a secure, agile software development process for cloud-native applications.

## Installation

1. Git Clone this repo
1. virtual enviroment `python3 -m venv ./.venv`
1. Install reqs `python3 -m  pip install -r requirements.txt`

## Authors and acknowledgment

1. Falko Sieverding - @fsiverding (GitLab Account)
2. Falko Sieverding - @UniversalAmateur (Private Account)

## License

For open source projects, say how it is licensed.

## Project status

WIP
